package com.egci428.egci428_project

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_log_in_page.*
import java.io.BufferedReader
import java.io.InputStreamReader
import java.lang.StringBuilder

class LogInPage : AppCompatActivity() { //Log in page.
    //The name of the file where we kept our username and password.
    private val file = "users.txt"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_log_in_page)

        logInBtn.setOnClickListener {
            val uname:String = usernameText.text.toString()
            val pword:String = passwordText.text.toString()

            //Check if username and password text field is not empty.
            if(!uname.isNullOrEmpty() && !pword.isNullOrEmpty()){ //If not empty, create a list of all username and password.
                try {
                    val fIn = openFileInput(file)
                    val mfile = InputStreamReader(fIn)
                    val br = BufferedReader(mfile)
                    var line = br.readLine()
                    val all = StringBuilder()
                    while(line != null){
                        all.append(line+"\n")
                        line = br.readLine()
                    }
                    br.close()
                    mfile.close()
                    val final_all = all.split('\n').toTypedArray()

                    for (i in final_all){ //Check the list with the current input.
                        if (i.contains(Regex(".*\\b${uname}\\b.*")) && i.contains(Regex(".*\\b${pword}\\b.*"))){ //If inputs exist, go to the next activity and toast a message.
                            val intent = Intent(this, MainMenu::class.java)
                            intent.putExtra("customerName", i.split(',').toTypedArray()[0])
                            startActivity(intent)
                            Toast.makeText(baseContext,"You are logged in.", Toast.LENGTH_SHORT).show()
                            break
                        } else { //If inputs doesn't exist, toast a message to try again.
                            Toast.makeText(baseContext,"Incorrect username or password.", Toast.LENGTH_LONG).show()
                        }
                    }
                } catch (e: Exception){
                    e.printStackTrace()
                }
            } else { //If empty, toast a message.
                Toast.makeText(baseContext,"Please fill in the username and password.", Toast.LENGTH_LONG).show()
            }
        }

        signUpBtn.setOnClickListener {//For new user to sign up.
            val intent = Intent(this,SignUpPage::class.java)
            startActivity(intent)
        }
    }
}