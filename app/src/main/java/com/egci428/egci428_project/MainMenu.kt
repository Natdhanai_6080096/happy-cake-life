package com.egci428.egci428_project

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.activity_main_menu.*

class MainMenu : AppCompatActivity() { //Main menu that greet a specific user.
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_menu)

        //receiving the customer name from the log in page.
        val bundle = intent.extras
        val customerName = bundle!!.getString("customerName")

        //Displaying the customer's name.
        welcomeText.text = "Welcome, ${customerName}"

        //Go to the order page once the user is ready.
        orderImageBtn.setOnClickListener {
            val intent = Intent(this, OrderPage::class.java)
            intent.putExtra("customerName",customerName)
            startActivity(intent)
        }
    }
}