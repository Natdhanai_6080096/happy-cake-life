package com.egci428.egci428_project

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_checkout_page.*

class CheckoutPage : AppCompatActivity() {
    private var test = arrayOf(0,1)
    private var totalPrice = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_checkout_page)

        //Receiving all the data from OrderPage activity.
        var customerName = intent.getStringExtra("customerName")
        var totalItem = intent.getStringExtra("totalItemCount")

        val cakesTotalPrice = arrayOf(
            intent.getStringExtra("cake1Count")!!.toInt() * 499,
            intent.getStringExtra("cake2Count")!!.toInt() * 399,
            intent.getStringExtra("cake3Count")!!.toInt() * 599,
            intent.getStringExtra("cake4Count")!!.toInt() * 299,
            intent.getStringExtra("cake5Count")!!.toInt() * 699
        )

        val cakesTotalAmount = arrayOf(
            intent.getStringExtra("cake1Count"),
            intent.getStringExtra("cake2Count"),
            intent.getStringExtra("cake3Count"),
            intent.getStringExtra("cake4Count"),
            intent.getStringExtra("cake5Count")
        )

        for (i in cakesTotalPrice){
            totalPrice += i
        }

        //Displaying the inputs.
        customerNameTextCheckout.text = "Customer: ${intent.getStringExtra("customerName")}"

        cake1Text.text = "${cakesTotalAmount[0]} (฿${cakesTotalPrice[0]})"
        cake2Text.text = "${cakesTotalAmount[1]} (฿${cakesTotalPrice[1]})"
        cake3Text.text = "${cakesTotalAmount[2]} (฿${cakesTotalPrice[2]})"
        cake4Text.text = "${cakesTotalAmount[3]} (฿${cakesTotalPrice[3]})"
        cake5Text.text = "${cakesTotalAmount[4]} (฿${cakesTotalPrice[4]})"

        totalText.text = "Total: ${totalItem} (฿ ${totalPrice})"

        if(totalPrice!! > 1000){ //If total price is higher than 1000, give the user a chance at receiving a discount.
            luckyDrawBtn.visibility = View.VISIBLE
        }

        luckyDrawBtn.setOnClickListener{//User only have one chance to try the lucky draw so we need to remove the button after they are finished.
            if (totalPrice != null) {
                if (totalPrice > 1000){
                    val intent = Intent(this, JapLot::class.java)
                    startActivity(intent)
                    luckyDrawBtn.visibility = View.GONE
                }
            }
        }

        deliveryBtn.setOnClickListener {//Once everything is ready, the user can choose a location at which they want the cake to be delivered.
            val intent = Intent(this, MapsActivity::class.java)
            intent.putExtra("cakesTotalPrice",cakesTotalPrice)
            intent.putExtra("cakesTotalAmount",cakesTotalAmount)
            intent.putExtra("customerName", customerName)

            intent.putExtra("cake1Amount",cakesTotalAmount[0])
            intent.putExtra("cake1Price",cakesTotalPrice[0])
            intent.putExtra("cake2Amount",cakesTotalAmount[1])
            intent.putExtra("cake2Price",cakesTotalPrice[1])
            intent.putExtra("cake3Amount",cakesTotalAmount[2])
            intent.putExtra("cake3Price",cakesTotalPrice[2])
            intent.putExtra("cake4Amount",cakesTotalAmount[3])
            intent.putExtra("cake4Price",cakesTotalPrice[3])
            intent.putExtra("cake5Amount",cakesTotalAmount[4])
            intent.putExtra("cake5Price",cakesTotalPrice[4])

            intent.putExtra("test",test)
            startActivity(intent)
        }
    }
}