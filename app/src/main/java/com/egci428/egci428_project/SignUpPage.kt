package com.egci428.egci428_project

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_log_in_page.*
import kotlinx.android.synthetic.main.activity_sign_up_page.*
import kotlinx.android.synthetic.main.activity_sign_up_page.passwordText
import java.util.*

class SignUpPage : AppCompatActivity() {
    private val file = "users.txt"
    private var data:String? = null
    private var uniqueID:String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up_page)

        //This button will call the saveData function that is responsible for saving the input to user.txt file.
        signup2Btn.setOnClickListener {
            saveData()
            val intent = Intent(this,LogInPage::class.java)
            startActivity(intent)
        }
    }

    private fun saveData() {
        //Take all the inputs.
        uniqueID = UUID.randomUUID().toString()
        val cusName = customerNameText.text.toString()
        val usr = userNameText.text.toString()
        val pwd = passwordText.text.toString()
        if (usr.isEmpty()) {
            userNameText.error = "Please enter a username"
            return
        }

        data = "${cusName}, ${usr}, ${pwd}, ${uniqueID}\n"

        try { //Write the inputs into the users.txt file.
            val fOut = openFileOutput(file, Context.MODE_APPEND)
            fOut.write(data!!.toByteArray())
            fOut.close()
            Toast.makeText(baseContext,"You have signed up!", Toast.LENGTH_LONG).show()
        } catch (e: Exception){
            e.printStackTrace()
        }

    }
}