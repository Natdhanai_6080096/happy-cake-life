package com.egci428.egci428_project

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.content.Intent
import android.opengl.Visibility
import android.util.Log
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.activity_luck.*

class JapLot : AppCompatActivity(), SensorEventListener {
    private var sensorManager: SensorManager? = null
    private var view:View? = null
    private var lastUpdate: Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_luck) //get layout

        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager //set sensor 
        lastUpdate = System.currentTimeMillis() // get current time on create

        backToCheckoutBtn.setOnClickListener {
            onBackPressed()
        }
    }


    override fun onResume() {
        super.onResume()
        sensorManager!!.registerListener(this, sensorManager!!.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) , SensorManager.SENSOR_DELAY_NORMAL) //set accelerometer sensor 
    }

    override fun onPause() {
        super.onPause()
        sensorManager!!.unregisterListener(this) //unregister sensor listener if pause 
    }

    override fun onSensorChanged(event: SensorEvent) {
        if(event.sensor.type == Sensor.TYPE_ACCELEROMETER){
            getAcceleroMeter(event) //if sensor type is accelerometer activate function
        }
    }

    override fun onAccuracyChanged(p0: Sensor?, p1: Int) {
    }

    private fun getAcceleroMeter(event: SensorEvent){
        val value = event.values
        val x = value[0] //get x value
        val y = value[1] //get y value
        val z = value[2] //get z value

        val Accel = (x*x+y*y+z*z)/(SensorManager.GRAVITY_EARTH*SensorManager.GRAVITY_EARTH) //calculate acceleration
        val actualTime = System.currentTimeMillis() //get current time
        if (Accel >= 1.7){
            if(actualTime - lastUpdate < 200){
                return
            }
            lastUpdate = actualTime 

            val image: ImageView = findViewById(R.id.image) //load image

            val clk_rotate = AnimationUtils.loadAnimation(
                this,
                R.anim.rotate_clockwise
            ) //load clockwise rotate animation

            image.startAnimation(clk_rotate) //make image rotate clockwise

            val random = (0..10).random().toString() //random number from 0 to 10
            if(random.equals("8")){
                text_view.text = "5% Discount!!!"
                image.setImageDrawable(
                    ContextCompat.getDrawable(
                        applicationContext, // Context
                        R.drawable.heart // Drawable
                    )
                )
                sensorManager!!.unregisterListener(this)
                backToCheckoutBtn.visibility = View.VISIBLE
            } //if 8 then win

            else{
                text_view.text = "Better Luck Next Time"
                image.setImageDrawable(
                    ContextCompat.getDrawable(
                        applicationContext, // Context
                        R.drawable.blue_heart // Drawable
                    )
                )
                sensorManager!!.unregisterListener(this)
                backToCheckoutBtn.visibility = View.VISIBLE
            } //any other number loses



        }
    }
}