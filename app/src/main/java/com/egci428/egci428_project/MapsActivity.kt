package com.egci428.egci428_project

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_maps.*

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {
    lateinit var dataReference: FirebaseFirestore

    private lateinit var mMap: GoogleMap
    private lateinit var locationManager: LocationManager
    private lateinit var locationListener: LocationListener
    private lateinit var currentLatLng: LatLng

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        dataReference = FirebaseFirestore.getInstance() //Starting firebase.

        //Receive all the necessary inputs.
        val cakesTotalPrice = arrayOf(
            intent.getIntExtra("cake1Price",0).toString(),
            intent.getIntExtra("cake2Price",0).toString(),
            intent.getIntExtra("cake3Price",0).toString(),
            intent.getIntExtra("cake4Price",0).toString(),
            intent.getIntExtra("cake5Price",0).toString()
        )

        val cakesTotalAmount = arrayOf(
            intent.getStringExtra("cake1Amount")!!.toString(),
            intent.getStringExtra("cake2Amount")!!.toString(),
            intent.getStringExtra("cake3Amount")!!.toString(),
            intent.getStringExtra("cake4Amount")!!.toString(),
            intent.getStringExtra("cake5Amount")!!.toString()
        )

        var customerName = intent.getStringExtra("customerName")

        //Once the user confirms the location, send all the important data to firebase.
        confirmBtn.setOnClickListener {
            val db = dataReference.collection("orders") //Creating a collection to store information. Much like a Table in MySQL.
            val orderID = db.document().id
            val messageData = Orders(
                orderID,
                customerName.toString(),
                "${cakesTotalAmount!![0]} (${cakesTotalPrice!![0]})",
                "${cakesTotalAmount!![1]} (${cakesTotalPrice!![1]})",
                "${cakesTotalAmount!![2]} (${cakesTotalPrice!![2]})",
                "${cakesTotalAmount!![3]} (${cakesTotalPrice!![3]})",
                "${cakesTotalAmount!![4]} (${cakesTotalPrice!![4]})",
                "${locationTextView.text}"
            )
            db.add(messageData)
                .addOnSuccessListener {
                    Toast.makeText(applicationContext,"Order sent!!", Toast.LENGTH_SHORT).show()
                }
                .addOnFailureListener {
                    Toast.makeText(applicationContext,"Failed to send order!", Toast.LENGTH_SHORT).show()
                }
            //Go to ThankYouPage once the button is tapped.
            val intent = Intent(this, ThankYouPage::class.java)
            startActivity(intent)
        }

        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager //Creating an instance for the manager to exist.
        locationListener = object : LocationListener{ //Implementing a listener.
            override fun onLocationChanged(location: Location) {
                currentLatLng = LatLng(location.latitude,location.longitude)
            }

            override fun onProviderDisabled(provider: String) { //Check if the provider is disabled. If so, then start it.
                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(intent)
            }
        }
        requestLocation()
    }

    private fun requestLocation() {
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){ //Check if permission is granted.
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 10)
            }
            return
        }
        locationManager.requestLocationUpdates("gps", 5000, 0f, locationListener)
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        //Show the current location of the user (in case he/she wants it to be deliver where he/she is).
        currentLocation.setOnClickListener {
            mMap.clear()
            mMap.addMarker(MarkerOptions().position(currentLatLng).title(currentLatLng.toString()))
            mMap.moveCamera(CameraUpdateFactory.newLatLng(currentLatLng))
            if(currentLatLng != null){
                locationTextView.text = "${currentLatLng.latitude}, ${currentLatLng.longitude}"
            }
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 16f))
        }

        //The user can long tap on the map to set a custom destination of his/her choice.
        mMap.setOnMapLongClickListener { latLng ->
            mMap.clear()
            mMap.addMarker(
                MarkerOptions()
                    .position(latLng)
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA)) //This is to change the marker's color.
                    .title("$latLng")
            )
            locationTextView.text = latLng.toString().replace("lat/lng:","").replace("(","").replace(")","")
        }
    }

    override fun onRequestPermissionsResult( //When user open the app for the first time, the app will need to request permission from the user first (just like with new app in iphone).
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when(requestCode){
            10 -> requestLocation()
            else -> { }
        }
    }

    override fun onPause() {
        super.onPause()
        locationManager.removeUpdates(locationListener)
    }
}