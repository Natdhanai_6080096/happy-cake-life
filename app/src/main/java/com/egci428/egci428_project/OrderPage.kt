package com.egci428.egci428_project

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_order_page.*

class OrderPage : AppCompatActivity() {
    private var totalItemCount: Int = 0
    private var cake1Count: Int = 0
    private var cake2Count: Int = 0
    private var cake3Count: Int = 0
    private var cake4Count: Int = 0
    private var cake5Count: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order_page)

        //Responding to user's tap of a cake.
        cake1.setOnClickListener {
            totalItemCount = 0
            cake1Count++
            totalItemCount += cake1Count + cake2Count + cake3Count + cake4Count + cake5Count
            itemCount.text = totalItemCount.toString()
            cake1CountText.text = cake1Count.toString()
        }

        cake2.setOnClickListener {
            totalItemCount = 0
            cake2Count++
            totalItemCount += cake1Count + cake2Count + cake3Count + cake4Count + cake5Count
            itemCount.text = totalItemCount.toString()
            cake2CountText.text = cake2Count.toString()
        }

        cake3.setOnClickListener {
            totalItemCount = 0
            cake3Count++
            totalItemCount += cake1Count + cake2Count + cake3Count + cake4Count + cake5Count
            itemCount.text = totalItemCount.toString()
            cake3CountText.text = cake3Count.toString()
        }

        cake4.setOnClickListener {
            totalItemCount = 0
            cake4Count++
            totalItemCount += cake1Count + cake2Count + cake3Count + cake4Count + cake5Count
            itemCount.text = totalItemCount.toString()
            cake4CountText.text = cake4Count.toString()
        }

        cake5.setOnClickListener {
            totalItemCount = 0
            cake5Count++
            totalItemCount += cake1Count + cake2Count + cake3Count + cake4Count + cake5Count
            itemCount.text = totalItemCount.toString()
            cake5CountText.text = cake5Count.toString()
        }

        //To reset all items to zero.
        resetBtn.setOnClickListener {
            totalItemCount = 0
            cake1Count = 0
            cake2Count = 0
            cake3Count = 0
            cake4Count = 0
            cake5Count = 0

            itemCount.text = "0"
            cake1CountText.text = "0"
            cake2CountText.text = "0"
            cake3CountText.text = "0"
            cake4CountText.text = "0"
            cake5CountText.text = "0"
        }

        //Once user is done, tap on this button to checkout his/her order.
        checkoutBtn.setOnClickListener {
            if(totalItemCount <= 0){ //check to make sure that the user at least select a cake. If no cakes are selected, toast a message.
                Toast.makeText(this,"Please select at least one cake.",Toast.LENGTH_SHORT).show()
            } else { //If there's at least one cake, send all the data to the next activity.
                val bundle = intent.extras
                val customerName = bundle!!.getString("customerName")
                val intent = Intent(this, CheckoutPage::class.java)
                intent.putExtra("cake1Count", cake1Count.toString())
                intent.putExtra("cake2Count", cake2Count.toString())
                intent.putExtra("cake3Count", cake3Count.toString())
                intent.putExtra("cake4Count", cake4Count.toString())
                intent.putExtra("cake5Count", cake5Count.toString())
                intent.putExtra("totalItemCount", totalItemCount.toString())
                intent.putExtra("customerName", customerName)
                startActivity(intent)
            }
        }
    }
}